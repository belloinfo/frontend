FROM node:18.14.2 AS build
WORKDIR /build

COPY ./ .

RUN npm i -g npm@9.5.1

RUN npm ci

RUN npm run build

FROM nginx
WORKDIR /usr/share/nginx/html
COPY --from=build /build/dist /usr/share/nginx/html
RUN chown -R www-data:www-data /usr/share/nginx/html
